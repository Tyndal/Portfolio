import React from "react";

const About = () => (
  <div className="home-hero-container">
    <div className="container">
      <h1 className="home-header-one">Cześć!</h1>
      <h2 className="home-header-two">Tu ma być coś o mnie</h2>
      <h2 className="home-header-three">Tylko muszę coś stworzyć</h2>
      <p className="home-hero-paragraph">Poproszę zatem o odrobinkę cierpliwości</p>
    </div>
  </div>
);

export default About;
