import React from "react";

const Portfolio = () => (
  <div className="container">
    <h1>Co zrobiłem</h1>
    <div className="portfolio-container">
      <a
        className="portfolio-item-link"
        href="http://awax.dev-tricks.pl"
        target="blank"
      >
        <div className="portfolio-item portfolio-item-1" />
      </a>
      <a
        className="portfolio-item-link"
        href="http://golden.dev-tricks.pl"
        target="blank"
      >
        <div className="portfolio-item portfolio-item-2" />
      </a>
      <a
        className="portfolio-item-link"
        href="http://kanban.dev-tricks.pl"
        target="blank"
      >
        <div className="portfolio-item portfolio-item-3" />
      </a>
      <div className="portfolio-item portfolio-item-4" />
      <div className="portfolio-item portfolio-item-5" />
      <div className="portfolio-item portfolio-item-6" />
    </div>
  </div>
);

export default Portfolio;
