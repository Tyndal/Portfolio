import React from "react";
//import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Home = () => (
  <div className="home-hero-container">
    <div className="container">
      <h1 className="home-header-one header">Cześć!</h1>
      <h2 className="home-header-two header">Nazywam się</h2>
      <h2 className="home-header-three header">Marcin Świrek</h2>
      <p className="home-hero-paragraph">...i zamienię na kod wszystko, co podpowie Ci wyobraźnia.</p>
    </div>
  </div>
);

export default Home;
