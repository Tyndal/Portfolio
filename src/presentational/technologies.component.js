import React from "react";

const images = [
  {
    id: 1,
    pic: "../images/html-5.svg",
    desc: "Html"
  },
  {
    id: 2,
    pic: "../images/css-3.svg",
    desc: "CSS"
  },
  {
    id: 3,
    pic: "../images/rwd.png",
    desc: "RWD"
  },
  {
    id: 4,
    pic: "../images/js.svg",
    desc: "JavaScript"
  },
  {
    id: 5,
    pic: "../images/jquery.svg",
    desc: "jQuery"
  },
  {
    id: 6,
    pic: "../images/sass.png",
    desc: "Sass"
  },
  {
    id: 7,
    pic: "../images/bootstrap.png",
    desc: "Bootstrap"
  },
  {
    id: 8,
    pic: "../images/bulma.svg",
    desc: "Bulma"
  },
  {
    id: 9,
    pic: "../images/ajax.png",
    desc: "Ajax"
  },
  {
    id: 10,
    pic: "../images/nodejs.png",
    desc: "Node.js"
  },
  {
    id: 11,
    pic: "../images/express.png",
    desc: "html"
  },
  {
    id: 12,
    pic: "../images/react.png",
    desc: "React"
  },
  {
    id: 13,
    pic: "../images/redux.png",
    desc: "Redux"
  },
  {
    id: 14,
    pic: "../images/webpack.png",
    desc: "Webpack"
  },
  {
    id: 15,
    pic: "../images/git.png",
    desc: "Git"
  },
  {
    id: 16,
    pic: "../images/npm.png",
    desc: "NPM"
  },
  {
    id: 17,
    pic: "../images/babel.png",
    desc: "Babel"
  },
  {
    id: 18,
    pic: "../images/mongo.png",
    desc: "MongoDB"
  }
];

// class Techno extends React.Component {
//   constructor(props) {
//     super(props);
//     render();
//     return <img src={this.props.images.pic} alt={this.props.images.desc} />;
//   }
// }

const Technologies = () => (
  <div className="container">
    <h2 className="techno-header">
      Technologie, z którymi, i nad którymi wciąż pracuję...
    </h2>
    <div className="techno-container">
      <div className="techno-img html-img" />
      <div className="techno-img css-img" />
      <div className="techno-img rwd-img" />
      <div className="techno-img js-img" />
      <div className="techno-img jquery-img" />
      <div className="techno-img sass-img" />
      <div className="techno-img bootstrap-img" />
      <div className="techno-img bulma-img" />
      <div className="techno-img ajax-img" />
      <div className="techno-img nodejs-img" />
      <div className="techno-img express-img" />
      <div className="techno-img react-img" />
      <div className="techno-img redux-img" />
      <div className="techno-img webpack-img" />
      <div className="techno-img git-img" />
      <div className="techno-img npm-img" />
      <div className="techno-img babel-img" />
      <div className="techno-img mongo-img" />
    </div>
  </div>
);

export default Technologies;

// export default Techno;
