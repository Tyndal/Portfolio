import React from "react";
import { Link } from "react-router";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";
import ResponsiveMenu from "react-responsive-navbar";
import Icon from "react-icons-kit";
import { close } from "react-icons-kit/fa/close";
import { menu } from "react-icons-kit/iconic/menu";

const Navigation = (props) => {
    return (
      <div>
      <ResponsiveMenu
        menuOpenButton={
          <div>
            <Icon icon={menu} />
          </div>
        }
        menuCloseButton={
          <div>
            <Icon icon={close} />
          </div>
        }
        changeMenuOn="1000px"
        largeMenuClassName="large-menu"
        smallMenuClassName="small-menu"
        menu={
          <div className="container">
            <ul className="navigation">
              <li>
                <Link to="/home">Start</Link>
              </li>
              <li>
                <Link to="/about">O mnie</Link>
              </li>
              <li>
                <Link to="/technologies">Technologie</Link>
              </li>
              <li>
                <Link to="/portfolio">Portfolio</Link>
              </li>
              <li>
                <Link to="/contact">Kontakt</Link>
              </li>
            </ul>
            {/* <div className="container-fluid">{props.children}</div> */}
          </div>
        }
      />
      <div className="container-fluid">{props.children}</div>
      </div>
    );
  }

// const Navigation = props => (
//   <div>
//     <nav className="navbar nav-tabs">
//       <div className="container">
//         <div className="navbar-header" />
//         <div className="collapse navbar-collapse">
//           <ul className="nav navbar-nav">
//             <li>
//               <Link to="/home">Start</Link>
//             </li>
//             <li>
//               <Link to="/about">O mnie</Link>
//             </li>
//             <li>
//               <Link to="/technologies">Technologie</Link>
//             </li>
//             <li>
//               <Link to="/portfolio">Portfolio</Link>
//             </li>
//             <li>
//               <Link to="/contact">Kontakt</Link>
//             </li>
//           </ul>
//         </div>
//       </div>
//     </nav>
//     <div className="container-fluid">{props.children}</div>
//   </div>
// );

export default Navigation;
