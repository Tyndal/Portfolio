import React, { Component } from "react";
import { render } from "react-dom";
import { Router, hashHistory } from "react-router";
import { Provider } from "react-redux";
import routes from "./routes";
import "./index.css";
import ResponsiveMenu from 'react-responsive-navbar';

render(
  <Provider>
    <div>
      <Router history={hashHistory} routes={routes} />
    </div>
  </Provider>,
  document.getElementById("root")
);
