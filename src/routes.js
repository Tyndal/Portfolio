import React from "react";
import { Route } from "react-router";
import Navigation from "./presentational/navigation.component";
import Home from "./presentational/home.component";
import About from "./presentational/about.component";
import NotFound from "./presentational/not-found.component";
import Technologies from "./presentational/technologies.component";
import Contact from "./presentational/contact.component";
import Portfolio from "./presentational/portfolio.component";

export default (
  <Route path="/" component={Navigation}>
    <Route path="home" component={Home} />
    <Route path="about" component={About} />
    <Route path="technologies" component={Technologies} />
    <Route path="contact" component={Contact} />
    <Route path="portfolio" component={Portfolio} />
    <Route path="*" component={NotFound} />
  </Route>
);
